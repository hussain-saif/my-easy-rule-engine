# README #

@author: Hussain Saif


This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The repository is for business rule implementation
* 0.0.1-SNAPSHOT


## Example

Domain: Loan
Rule 1: A person is eligible for home loan?
if:
   1. He has monthly salary more than 50K.
   2. And his credit score is more than 800.
   3. And requested loan amount is less than 40L.
   4. And bank's current year target not done for home loan.
then:
   1. Approve the home loan.
   2. Sanction 80% of requested loan amount.
   
   
Rule 2: A person is eligible for home loan?
if:
   1. He has monthly salary more than 35K and less than 50K.
   2. And his credit score is less than 500.
   3. And requested loan amount is less than 20L.
   4. And bank's current year target not done for home loan.
then:
   1. Approve home loan.
   2. Sanction 60% of requested loan amount.
   
## Rules in Mvel Form

#Loan Rule 1:
Condition:
     input.monthlySalary >= 50000 
     && input.creditScore >= 800 
     && input.requestedLoanAmount < 4000000    
Action:
     output.setApprovalStatus(true);
     output.setSanctionedPercentage(90); 
     output.setProcessingFees(8000);
     
#Loan Rule 2:
Condition:
     input.monthlySalary >= 35000 && input.monthlySalary <= 50000
     && input.creditScore <= 500 
     && input.requestedLoanAmount < 2000000     
Action:
     output.setApprovalStatus(true);
     output.setSanctionedPercentage(60); 
     output.setProcessingFees(2000);
 
# Sample Input

{
"userInfo": {
	"firstName": "Mark",
    "lastName": "K",
    "age": 25,
    "bankName": "ABC BANK",
    "accountNo": "4531625"
    },
  "monthlySalary": 70000.0,
  "creditScore": 900,
  "requestedLoanAmount": 3500000.0
}

# References 

- https://github.com/j-easy/easy-rules
- https://medium.com/@er.rameshkatiyar/implement-your-own-rule-engine-java8-springboot-mvel-5928474e1ba5

