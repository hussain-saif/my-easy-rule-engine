package my.walnut.re.controller;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import my.walnut.re.model.LoanDetails;
import my.walnut.re.model.UserDetails;
import my.walnut.re.model.UserInfo;
import my.walnut.re.service.KnowledgeBaseService;
import my.walnut.re.service.KnowledgeBaseService.RULE;
import my.walnut.re.service.LoanService;

@RestController
public class RuleEngineRestController {

	@Autowired
	LoanService loanService;

	@Autowired
	KnowledgeBaseService knowledgeBaseService;

	@GetMapping(value = "/get-all-rules")
	public ResponseEntity<?> getAllRules() {
		List<RULE> allRules = knowledgeBaseService.getAllRuleNames();
		return ResponseEntity.ok(allRules);
	}
	
	@PostMapping(value = "/loan")
	public ResponseEntity<?> runLoanRuleEngine(@RequestBody UserDetails userDetails) throws FileNotFoundException, Exception {			
				
		List<UserDetails> details = new ArrayList<UserDetails>();
		details.add(userDetails);
		
		return ResponseEntity.ok(processLoan(details));
	}
	
	@PostMapping(value = "/mock_loan")
	public ResponseEntity<?> runMockLoanRuleEngine() throws FileNotFoundException, Exception {		
		
		List<UserDetails> details = new ArrayList<UserDetails>();
		details.add(getMockUserDetail());
		details.add(getMockUserDetail2());
		details.add(getMockUserDetail3());		
		
		return ResponseEntity.ok(processLoan(details));
	}

	private List<LoanDetails> processLoan(List<UserDetails> details)
	{
		List<LoanDetails> loanDetails = new ArrayList<LoanDetails>();
		
		details.stream().forEach( detail -> {
			LoanDetails result = null;
			try {
				result = (LoanDetails) loanService.processLoan(detail);
			} catch (Exception e) {
				e.printStackTrace();
			}
			loanDetails.add(result);
		});
		return loanDetails;
	}
	
	private UserDetails getMockUserDetail() {	 
		
		//- User-1
		UserInfo userInfo = new UserInfo();
		userInfo.setAge(25);
		userInfo.setFirstName("Mark");
		userInfo.setLastName("K");
		userInfo.setBankName("ABC BANK");
		userInfo.setAccountNo("4531625");

		UserDetails userDetails = new UserDetails();
		userDetails.setUserInfo(userInfo);
		userDetails.setMonthlySalary(70000.0);
		userDetails.setCreditScore(900);
		userDetails.setRequestedLoanAmount(3500000.0);

		return userDetails;
	}
	
	private UserDetails getMockUserDetail2() {

		//- User-2
		UserInfo userInfo = new UserInfo();
		userInfo.setAge(25);
		userInfo.setFirstName("Karnon");
		userInfo.setLastName("M");
		userInfo.setBankName("ABC BANK");
		userInfo.setAccountNo("131625");

		UserDetails userDetails = new UserDetails();
		userDetails.setUserInfo(userInfo);
		userDetails.setMonthlySalary(40000.0);
		userDetails.setCreditScore(300);
		userDetails.setRequestedLoanAmount(1500000.0);

		return userDetails;
	}
	
	private UserDetails getMockUserDetail3() {

		//- User-3
		UserInfo userInfo = new UserInfo();
		userInfo.setAge(25);
		userInfo.setFirstName("Sharon");
		userInfo.setLastName("Mae");
		userInfo.setBankName("ABC BANK");
		userInfo.setAccountNo("766535");

		UserDetails userDetails = new UserDetails();
		userDetails.setUserInfo(userInfo);
		userDetails.setMonthlySalary(100.0);
		userDetails.setCreditScore(300);
		userDetails.setRequestedLoanAmount(1500000.0);

		return userDetails;
	}

}
