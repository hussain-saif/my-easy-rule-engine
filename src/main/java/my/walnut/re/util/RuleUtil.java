package my.walnut.re.util;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;

public class RuleUtil {

	public static Rule findRuleByName(String ruleName, Rules rules) {
		List<Rule> ruleList = StreamSupport.stream(rules.spliterator(), false).collect(Collectors.toList());

		return ruleList.stream().filter(rule -> rule.getName().equalsIgnoreCase(ruleName)).findFirst().orElse(null);
	}

	
}
