package my.walnut.re.service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

@Service
public class KnowledgeBaseService {

	@Autowired
	ResourceLoader resourceLoader;

	
	private Map<RULE, String> ruleMap = new HashMap<RULE, String>();
	public static final String UW_FOLDER_PATH = File.separator + "rules" + File.separator;
	public static final String EXTENSION = ".yml";

	public enum RULE {
		LOAN_RULE_1, LOAN_RULE_2, LOAN_RULE_3;
	}

	public KnowledgeBaseService() {		
	}

	@PostConstruct
	public void loadMapping() throws Exception {		
		ruleMap.put(RULE.LOAN_RULE_1, getFilePath("loan-rule-1.yml"));
		ruleMap.put(RULE.LOAN_RULE_2, getFilePath("loan-rule-2.yml"));
		ruleMap.put(RULE.LOAN_RULE_3, getFilePath("loan-rule-3.yml"));
	}



	private String getFilePath(String fileName) throws Exception{
		Resource resource = resourceLoader.getResource("classpath:rules/" + fileName);		
		return resource.getFile().getPath();		
	}
 
	public List<String> getValueByName(String value) {
		return ruleMap.keySet().stream().filter(v -> v.name().contains(value.toUpperCase())).map(m -> ruleMap.get(m))
				.collect(Collectors.toList());
	}

	public List<RULE> getAllRuleNames() {
		return ruleMap.keySet().stream().collect(Collectors.toList());
	}

	public Map<RULE, String> getRuleMap() {
		return ruleMap;
	}
}
