package my.walnut.re.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.api.RulesEngineParameters;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.mvel.MVELRuleFactory;
import org.jeasy.rules.support.reader.YamlRuleDefinitionReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import my.walnut.re.controller.RuleEngineRestController;
import my.walnut.re.model.LoanDetails;
import my.walnut.re.model.UserDetails;

@Service
public class LoanService {
	private static final Logger logger = LoggerFactory.getLogger(LoanService.class);
	public static final String UW_FOLDER_PATH = File.separator + "rules" + File.separator;
	public static final String EXTENSION = ".yml";

	@Autowired
	KnowledgeBaseService knowledgeBaseService;
	
	public LoanDetails processLoan(UserDetails userDetails) throws FileNotFoundException, Exception {

		logger.info("method: processLoan() in progress");
		
		LoanDetails loanDetails = new LoanDetails();
		loanDetails.setUserInfo(userDetails.getUserInfo());

		Facts facts = new Facts();
		facts.put("input", userDetails);
		facts.put("output", loanDetails);

		// create MVEL Factory		
		MVELRuleFactory factory = new MVELRuleFactory(new YamlRuleDefinitionReader());
		Rules rules = new Rules();
		for (String filePath : knowledgeBaseService.getValueByName("Loan")) {
			Rules newRules = factory.createRules(new FileReader(filePath));
			for (Rule rule : newRules) {
				rules.register(rule);
			}
		}

		RulesEngineParameters rulesEngineParameters = new RulesEngineParameters();
		rulesEngineParameters.setSkipOnFirstAppliedRule(true);
		// create a default rules engine and fire rules on known facts
		RulesEngine rulesEngine = new DefaultRulesEngine(rulesEngineParameters);
		rulesEngine.getParameters();
		rulesEngine.fire(rules, facts);

		return loanDetails;
	}

}
