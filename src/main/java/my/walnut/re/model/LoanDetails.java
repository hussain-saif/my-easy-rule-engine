package my.walnut.re.model;

public class LoanDetails {
	
    private UserInfo userInfo;
	private Boolean approvalStatus;
    private Float sanctionedPercentage;
    private Double processingFees;
    	
    public Boolean getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(Boolean approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	public Float getSanctionedPercentage() {
		return sanctionedPercentage;
	}
	public void setSanctionedPercentage(Float sanctionedPercentage) {
		this.sanctionedPercentage = sanctionedPercentage;
	}
	public Double getProcessingFees() {
		return processingFees;
	}
	public void setProcessingFees(Double processingFees) {
		this.processingFees = processingFees;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
    
	
    
}